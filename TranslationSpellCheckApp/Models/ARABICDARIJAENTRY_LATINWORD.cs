﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace TranslationSpellCheckApp.Models
{
    [Serializable]
    [XmlRoot("ARABICDARIJAENTRY_LATINWORD"), XmlType("ARABICDARIJAENTRY_LATINWORD")]
    public class ARABICDARIJAENTRY_LATINWORD
    {
        public Guid ID_ARABICDARIJAENTRY_LATINWORD { get; set; }    // PK
        public Guid ID_ARABICDARIJAENTRY { get; set; }              // FK one-to-many
        public String LatinWord { get; set; }
        public int VariantsCount { get; set; }
        public String MostPopularVariant { get; set; }
    }
}