﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace TranslationSpellCheckApp.Models
{
    [Serializable]
    [XmlRoot("ARABICDARIJAENTRY"), XmlType("ARABICDARIJAENTRY")]
    public class ARABICDARIJAENTRY
    {
        public Guid ID_ARABICDARIJAENTRY { get; set; }
        public Guid ID_ARABIZIENTRY { get; set; }
        public String ArabicDarijaText { get; set; }
    }
}