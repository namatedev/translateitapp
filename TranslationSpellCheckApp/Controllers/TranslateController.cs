﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace TranslationSpellCheckApp.Controllers
{
    public class TranslateController : Controller
    {
        // GET: Translate
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult TranslateText(string sourcelanguage, string targetlanguage, string sourceText)
        {
            string url = "https://translation.googleapis.com/language/translate/v2?key=AIzaSyBL3KTZOjRGeND8GVBYw7FjzVb0WcYKECw";
            url += "&source=" + sourcelanguage;
            url += "&target=" + targetlanguage;
            url += "&q=" + Server.UrlEncode(sourceText.Trim());
            try
            {
                WebClient client = new WebClient();
                client.Encoding = System.Text.Encoding.UTF8;
                string json = client.DownloadString(url);
                JsonData jsonData = (new JavaScriptSerializer()).Deserialize<JsonData>(json);
                var TranslatedText = jsonData.Data.Translations[0].TranslatedText;
                return Json(new { success = true, Text = TranslatedText });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, Text = ex.InnerException });
            }

        }

        public class JsonData
        {
            public Data Data { get; set; }
        }

        public class Data
        {
            public List<Translation> Translations { get; set; }
        }

        public class Translation
        {
            public string TranslatedText { get; set; }
        }
    }
}