﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TranslationSpellCheckApp.Models;
using static TranslationSpellCheckApp.Controllers.TranslateController;

namespace TranslationSpellCheckApp.Controllers
{
    public class HomeController : Controller
    {
        String spellCheckAPi = "https://api.cognitive.microsoft.com/bing/v5.0/spellcheck?text=";
        String bingSpellApiKey = ConfigurationManager.AppSettings["BingSpellcheckAPIKey"].ToString();

        public ActionResult Index()
        {
            ViewBag.ArabicWords = RetrunListOfArabicWords();
            return View();
        }

        public List<ARABICDARIJAENTRY> RetrunListOfArabicWords()
        {
            string arabicWordFile = Server.MapPath("~/ARABICDARIJAENTRY/data_M_ARABICDARIJAENTRY.xml");//Path of the xml script  
            DataSet arabicDS = new DataSet();//Using dataset to read xml file  
            arabicDS.ReadXml(arabicWordFile);
            var ArabicWords = new List<ARABICDARIJAENTRY>();
            ArabicWords = (from rows in arabicDS.Tables[0].AsEnumerable()
                           select new ARABICDARIJAENTRY
                           {
                               ID_ARABICDARIJAENTRY = new Guid(rows[0].ToString()), //Convert row to int  
                               ID_ARABIZIENTRY = new Guid(rows[1].ToString()),
                               ArabicDarijaText = rows[2].ToString(),
                           }).ToList();
            return ArabicWords;
        }

        public JsonResult TranslateText(string ID_ARABICDARIJAENTRY, string ID_ARABIZIENTRY, string ArabicDarijaText)
        {
            var previousText = "";
            var translatedSentence = "";

            string latinWordsFile = Server.MapPath("~/ARABICDARIJAENTRY_LATINWORD/data_M_ARABICDARIJAENTRY_LATINWORD.xml");//Path of the latin xml file  
            DataSet latinDS = new DataSet(); // Using dataset to read xml file 
            latinDS.ReadXml(latinWordsFile);

            string arabicWordFile = Server.MapPath("~/ARABICDARIJAENTRY/data_M_ARABICDARIJAENTRY.xml");//Path of the xml script  
            DataSet arabicDS = new DataSet(); // Using dataset to read xml file  
            arabicDS.ReadXml(arabicWordFile);

            DataTable dtLatinWords = latinDS.Tables[0];
            foreach (DataRow dr in dtLatinWords.AsEnumerable())
            {
                var test = dr[1].ToString();
                if (dr[1].ToString() == ID_ARABICDARIJAENTRY)
                {
                    previousText = dr[2].ToString();
                    string spellcheckUrl = spellCheckAPi + previousText + "&mode=proof";
                    var correctedWord = bingSpellcheckApi(spellcheckUrl, bingSpellApiKey);
                    if (correctedWord != previousText)
                    {
                        if (correctedWord == "")
                            correctedWord = previousText;
                        var TranslatedText = getArabicTranslatedWord(correctedWord);
                        ArabicDarijaText = ArabicDarijaText.Replace(previousText, TranslatedText);
                    }
                }
            }

            return Json(new { success = true, Text = ArabicDarijaText });
        }

        public string bingSpellcheckApi(string url, string apiKey)
        {
            var correctedText = "";
            WebClient client = new WebClient();
            client.Headers.Add("Ocp-Apim-Subscription-Key", apiKey);
            client.Encoding = System.Text.Encoding.UTF8;
            try
            {
                string json = client.DownloadString(url);
                var jsonresult = JObject.Parse(json).SelectToken("flaggedTokens") as JArray;
                foreach (var result in jsonresult)
                {
                    correctedText = result.SelectToken("suggestions[0].suggestion").ToString();
                    return correctedText;
                }
                return correctedText;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string getArabicTranslatedWord(string correctedWord)
        {
            string translationApiKey = ConfigurationManager.AppSettings["GoogleTranslationApiKey"].ToString();
            string translateApiUrl = "https://translation.googleapis.com/language/translate/v2?key=" + translationApiKey;
            translateApiUrl += "&target=" + "AR";
            translateApiUrl += "&q=" + Server.UrlEncode(correctedWord.Trim());
            WebClient client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;
            string Jsonresult = client.DownloadString(translateApiUrl);
            JsonData jsonData = new JsonData();
            jsonData = (new JavaScriptSerializer()).Deserialize<JsonData>(Jsonresult);
            var TranslatedText = jsonData.Data.Translations[0].TranslatedText;
            translateApiUrl = "";
            return TranslatedText;
        }
    }
}